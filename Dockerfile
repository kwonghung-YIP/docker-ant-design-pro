FROM node:carbon

RUN npm install ant-design-pro-cli -g
RUN cd /root; \
    git clone --depth=1 https://github.com/ant-design/ant-design-pro.git my-project; \
	cd my-project; \
	npm install

WORKDIR /root/my-project
